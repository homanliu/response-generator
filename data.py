# -*- coding: utf-8 -*-

form_id = '1Z2ynV5LZh55d5WDS1RVEjpVIPSiEln7Hq9zu7kXkqrU'

name_id = 'entry.934152323'
gender_id = 'entry.660187630'
school_id = 'entry.711842581'
year_id = 'entry.407077880'
mc_id = [
    'entry.1057010836',
    'entry.747507340',
    'entry.1709762102',
    'entry.590214155',
    'entry.755482434',
    'entry.888866503',
    'entry.2030018874',
    'entry.1954251683',
    'entry.288290605'
]

gender_ans = ['男', '女']
school_ans = [
    '香港大學',
    '香港中文大學',
    '香港科技大學',
    '香港城市大學',
    '香港理工大學',
    '香港浸會大學',
    '香港樹仁大學',
    '香港公開大學',
    '香港嶺南大學',
    '香港教育大學'
]
year_ans = [1, 2, 3, 4, 5, 6]
mc_ans = [
    '完全不同意',
    '非常不同意',
    '不同意',
    '中立',
    '同意',
    '非常同意',
    '完全同意'
]

