# -*- coding: utf-8 -*-
# https://eureka.ykyuen.info/2014/07/30/submit-google-forms-by-curl-command/

from data import *
import random
import requests

name = 'Test'
gender = random.choice(gender_ans)
school = random.choice(school_ans)
intake_year = random.choice(year_ans)

mc = []
for i in range(9):
    mc.append(random.choice(mc_ans))

output = {'ifq': ''}
output[name_id] = name
output[gender_id] = gender
output[school_id] = school
output[year_id] = str(intake_year)
for i in range(9):
    output[mc_id[i]] = mc[i]
output['submit'] = 'Submit'
print (output)

url = 'https://docs.google.com/forms/d/' + form_id + '/formResponse'
headers = {'Accept-Charset': 'UTF-8'}
print (url)

r = requests.post(url, headers=headers, params=output)
print (r)
print ('')
print (r.encoding)
print ('')
print (r.url)
print ('')